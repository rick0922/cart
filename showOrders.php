<?php
session_start();
require("orderModel.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
</head>
<body>
<p>This is the MAIN page</p>
<hr>
<?php
echo "Hello ", $_SESSION["loginProfile"]["id"],
", Your level is: ", $_SESSION["loginProfile"]["level"],"<HR>";
?>
	<table width="200" border="1">
  <tr>
    <td>order ID</td>
    <td>Date</td>
    <td>Status</td>
  </tr>
<?php
$result=getOrderDetail($_SESSION["loginProfile"]["id"]);
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['ordID'] . "</td>";
	echo "<td>{$rs['date']}</td>";
	echo "<td>" , $rs['status'], "</td>";
	//echo "<td><a href='addToCart.php?prdID='" . $rs['prdID'] . "'>Add</a></td>";
	echo "</tr>";
}
?>
</table>
<a href="main.php">OK</a><hr>

</body>
</html>
