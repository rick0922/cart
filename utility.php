<?php
session_start();

function checkLogin($minLevel, $url) {
    if($minLevel>=0) {
        if(isSet($_SESSION["loginProfile"])){
            header("Location:loginUI.php");
        }
    }
    if($_SESSION['loginProfile']['level']<$minLevel) {
        if(isSet($_SESSION["loginProfile"])){
            header("Location:"+$url);
        }
    }
}
?>