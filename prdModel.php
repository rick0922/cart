﻿<?php
require_once("dbconfig.php");

function getPrdList() {
    global $db;
    $sql = "SELECT id,name, price FROM product order by id";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    //mysqli_stmt_bind_param($stmt, "ss", $ID, $passWord); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}

function addItem($name, $price){
    global $db;
    $sql = "insert into product (name, price) values (?,?)";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "si", $name, $price); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}

function removeItem($prdID){
    global $db;
    $sql = "delete from product where id=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $prdID); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}

function getItem($prdID){
    global $db;
    $sql = "select name, price from product where id=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $prdID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt);
    return $result;
}

function updateItem($name, $price, $prdID){
    global $db;
    $sql = "update product set name=?, price=? where id=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "sii", $name, $price, $prdID); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}

?>










