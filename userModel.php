﻿<?php
require_once("dbconfig.php");

function getUserProfile($id, $pw) {
    global $db;
    $sql = "SELECT id, level  FROM user WHERE id=? and pw=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "ss", $id, $pw); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results		
    if ($row=mysqli_fetch_assoc($result)) {
        //return user profile
        $ret=array('id' => $id, 'level' => $row['level']);
    } else {
	//ID, password are not correct
	$ret=NULL;
    }
    return $ret;
}


function addUserProfile($id, $pw){
    global $db;
    $sql = "insert into user (id, pw, level) values (?,?,0)";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "ss", $id, $pw); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}

?>










