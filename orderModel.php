﻿<?php
require_once("dbconfig.php");

function getOrderList($uID) {
	global $db;
	$sql = "SELECT ordID, date, status FROM userOrder WHERE uID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getConfirmedOrderList() {
	global $db;
	$sql = "SELECT ordID, uID, date FROM userOrder WHERE status=1";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function _getCartID($uID) {
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "INSERT into userOrder ( uID, status ) values (?,0)";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}

function addToCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
    if(mysqli_fetch_assoc(checkOrder($prdID, $ordID))){
		echo "+1";
	    $sql = 'UPDATE orderItem set quantity = quantity+1 where ordID=? and prdID=?';
	    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
	    mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
    	mysqli_stmt_execute($stmt);  //執行SQL
	} else {
		echo "new", $ordID, $prdID;
	    $sql = "INSERT into orderItem (ordID, prdID, quantity) values (?,?,1)";
	    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
	    mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
	    mysqli_stmt_execute($stmt);  //執行SQL
    }
}

function removeFromCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
	$result=checkOrder($prdID, $ordID);
	while (	$rs=mysqli_fetch_assoc($result)) {
        if($rs['quantity']!=1){
	        $sql = 'UPDATE orderItem set quantity = quantity-1 where ordID=? and prdID=?';
	        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
	        mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
    	    mysqli_stmt_execute($stmt);  //執行SQL
	    } else {
	        $sql = "DELETE from orderItem where ordID=? and prdID=?";
	        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
	        mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
	        mysqli_stmt_execute($stmt);  //執行SQL
	    }
    }
}

function checkout($uID, $address) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "UPDATE userorder set date=now(),address=?,status=1 where ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "si", $address, $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function shipout($ordID) {
	global $db;
	$sql = "UPDATE userorder set status=2 where ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="SELECT product.id, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.id and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}


function getOrderDetail($uID) {
	global $db;
	$sql="SELECT ordID, date, status from userOrder where uID=?";
	$stmt = mysqli_prepare($db, $sql);
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getOrderDetailByAdmin($ordID) {
	global $db;
	$sql="SELECT id, name, price, quantity from orderitem, product where ordID=? and prdID=id";
	$stmt = mysqli_prepare($db, $sql);
	mysqli_stmt_bind_param($stmt, "s", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function checkOrder($prdID, $ordID) {
	global $db;
	$sql="SELECT quantity from orderItem where prdID=? and ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $prdID, $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
?>










